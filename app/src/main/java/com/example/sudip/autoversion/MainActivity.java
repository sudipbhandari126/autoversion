package com.example.sudip.autoversion;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button versionButton = (Button) findViewById(R.id.version_button);

        versionButton.setOnClickListener (new Button.OnClickListener() {
            public void onClick(View v)   {
                //toast message
                //Context context = getApplicationContext();
                //CharSequence text = "Version info";
                //int duration = Toast.LENGTH_SHORT;
                //Toast toast = Toast.makeText(context,text,duration);
                //toast.show();
                //toast version info
                try {
                final PackageManager pm = getPackageManager();
                String apkName = "app-release.apk";
                String fullPath = Environment.getExternalStorageDirectory()+"/"+apkName;
                    //Toast.makeText(getApplicationContext(),fullPath,Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(),"what",Toast.LENGTH_SHORT).show();
                    PackageInfo info = pm.getPackageArchiveInfo(fullPath,0);
                    Toast.makeText(getApplicationContext(), "Version="+info.versionCode, Toast.LENGTH_LONG).show();


                } catch (Exception e)
                {
                    Log.i("Exception",e.toString());
                }

            }
        });

    }
}
